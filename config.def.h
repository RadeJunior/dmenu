/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int min_width = 500;                    /* minimum width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"JetbrainsMonoMedium Nerd Font:size=9",
	"Hack Nerd Font:size=9"
};
static const char *prompt      = "Run:";      /* -p  option; prompt to the left of input field */

static const char col_bg[]          = "#282a36";
static const char col_fg[]          = "#ffffff";
static const char col_magenta[]     = "#A77AC4";
static const char col_blue[]        = "#7197E7";

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { col_fg, col_bg },
	[SchemeSel] = { col_fg, col_magenta },
	[SchemeSelHighlight] = { col_bg, col_magenta },
	[SchemeNormHighlight] = { col_magenta, col_bg },
	[SchemeOut] = { "#000000", "#00ffff" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
